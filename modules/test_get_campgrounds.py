import pytest
from get_campgrounds import get_campgrounds_response
from setup_api_key import get_api_key

@pytest.fixture
def state_fixture():
    state_cd = 'GA'
    return state_cd


def test_get_campgrounds_response(state_fixture):
    '''test to make sure we get back data'''
    api_key = get_api_key()
    test_response = get_campgrounds_response(state_fixture, api_key)
    assert test_response

def test_get_campgrounds_response_exception(state_fixture):
    with pytest.raises(Exception):
        get_campgrounds_response(state_fixture, 'test_key')