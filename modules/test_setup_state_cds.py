import pytest
from setup_state_cds import get_state_cds


def test_get_state_cds():
    '''test to make sure we get back a key'''
    state_codes = get_state_cds()

    assert state_codes
    assert len(state_codes) == 56