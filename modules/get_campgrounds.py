import requests


def get_campgrounds_response(state_cd, api_key):
    campgrounds_api_url = 'https://developer.nps.gov/api/v1/campgrounds'
    camp_params = {
        'api_key': api_key,
        'stateCode': state_cd
    }
    camp_response = requests.get(campgrounds_api_url, params=camp_params)
    if camp_response.status_code != 200:
        raise Exception(f'Campgrounds API Status code: {camp_response.status_code}')
    camp_response_json = camp_response.json()

    # responses = [['Campground', 'Description', 'Reservation URL', 'Fees']]
    responses = []
    for campground in camp_response_json['data']:
        response = []
        response.append(campground['name'])
        response.append(campground['description'])
        if len(campground['fees']) == 0:
            response.append(f'Reservation: {campground['reservationUrl']}')
        else:
            fee_string = ''
            for fee in campground['fees']:
                fee_string = fee_string + f'{fee['title']}: ${fee['cost']}\n'
            response.append(f'{fee_string}\nReservation: {campground['reservationUrl']}')
        if len(campground['images']) == 0:
            response.append('')
        else:
            response.append(campground['images'][0]['url'])

        responses.append(response)

    return responses


# test = get_campgrounds_response('ga', '')
# print(test)