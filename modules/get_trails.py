import requests


def get_thingstodo_response(state_cd, api_key):
    thingstodo_api_url = 'https://developer.nps.gov/api/v1/thingstodo'
    hike_params = {
        'api_key': api_key,
        'stateCode': state_cd,
        'q': 'BFF8C027-7C8F-480B-A5F8-CD8CE490BFBA'     # activity ID for hiking
    }
    hike_response = requests.get(thingstodo_api_url, params=hike_params)
    if hike_response.status_code != 200:
        raise Exception(f'Campgrounds API Status code: {hike_response.status_code}')
    hike_response_json = hike_response.json()

    # responses = [['Trail', 'Description and URL', 'Duration', 'Are There Trail Fees', 'Are Pets Allowed']]
    responses = []
    for trail in hike_response_json['data']:
        response = []
        response.append(trail['title'])
        response.append(f'{trail['shortDescription']}\n\n{trail['url']}')
        response.append(trail['duration'])
        response.append(trail['doFeesApply'])
        response.append(trail['arePetsPermitted'])
        response.append(trail['images'][0]['url'])

        responses.append(response)

    return responses


# test = get_thingstodo_response('ga','')
# print(test)