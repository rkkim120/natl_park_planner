import pytest
from get_trails import get_thingstodo_response
from setup_api_key import get_api_key

@pytest.fixture
def state_fixture():
    state_cd = 'GA'
    return state_cd


def test_get_thingstodo_response(state_fixture):
    '''test to make sure we get back data'''
    api_key = get_api_key()
    test_response = get_thingstodo_response(state_fixture, api_key)
    assert test_response

def test_get_thingstodo_response_exception(state_fixture):
    with pytest.raises(Exception):
        get_thingstodo_response(state_fixture, 'test_key')