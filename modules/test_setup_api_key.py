import pytest
from setup_api_key import get_api_key


def test_get_api_key():
    '''test to make sure we get back a key'''
    api_key = get_api_key()

    assert api_key
    assert len(api_key) == 40