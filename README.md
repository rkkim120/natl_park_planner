# National Park Planner
![Preview of GA Campgrounds](images/preview.png)

## Description
Visiting a state and wondering if there are any hiking trails or campgrounds that are maintained by the National Park Service?  Use this app to choose a state and view everything in an easy-to-read table.

This uses the NPS API: <https://www.nps.gov/subjects/developer/api-documentation.htm> 

The dependencies can be found in `requirements.txt`. 

## Usage
1. Clone this repo and navigate to the folder in your terminal. 
1. Make sure to add/update the API key in `modules/setup_api_key.py` to have access to the NPS API.
1. Create a venv, activate, and run `pip install -r requirements.txt` to install dependencies.
1. Run by using `python .\planner_ui.py` in your terminal.  A new window should pop-up with the user interface.
1. Choose a state and activity in the dropdowns and click submit!
1. A new tab should appear to the right with the results.  Every new search will open a new tab.  If you search something that you've already searched, it will switch to that specific results tab.

## Roadmap
Some stretch goals:
- Make any URLs into hyperlinks
- Unit tests for UI
- Scenic Driving activity
- Responses contain latlong, might be able to get current weather forecast