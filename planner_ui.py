from modules.get_campgrounds import get_campgrounds_response
from modules.get_trails import get_thingstodo_response
from modules.setup_state_cds import get_state_cds
from modules.setup_api_key import get_api_key
# to open images --------------------------------------------------------
from urllib.request import urlopen
from PIL import Image, ImageTk
import ssl
ssl._create_default_https_context = ssl._create_unverified_context
from urllib.error import HTTPError
# for UI ----------------------------------------------------------------
import customtkinter
from CTkTable import CTkTable


######################################################################################
# functions used by UI
######################################################################################
def get_camping_response(state_cd, api_key):
    '''invoke campgrounds API and display results'''
    # check if results for that state were already retrieved
    try:
        results_tabview.set(f'Loading Campgrounds in {state_cd}...')
    except:
        # get inputs and invoke apis
        camp_responses_formatted = get_campgrounds_response(state_cd, api_key)

        # create new tab and frame
        camp_result_tab = results_tabview.add(f'Loading Campgrounds in {state_cd}...')
        results_tabview.set(f'Loading Campgrounds in {state_cd}...')
        camping_frame = customtkinter.CTkScrollableFrame(master=camp_result_tab, height=900, width=1300)
        camping_frame.pack(padx=10, pady=10)

        # output results
        camp_headers = [['Campground', 'Description', 'Fees and Reservation URL']]
        camp_results = CTkTable(master=camping_frame, values=camp_headers, width=1, wraplength=500, header_color="#2A8C55")
        camp_results.edit_row(0, text_color="#fff", hover_color="#2A8C55")
        camp_results.pack(expand=True)

        row_num = 1
        for response in camp_responses_formatted:
            image_url = str(response.pop())
            camp_results.add_row(response)

            if image_url:
                try:
                    u = urlopen(image_url)
                    raw_data = u.read()
                    u.close()
                    imagetk = ImageTk.PhotoImage(data=raw_data)
                    image = ImageTk.getimage(imagetk)
                    camp_img = customtkinter.CTkImage(light_image=image, size=(150,150))
                    camp_results.insert(row=row_num, column=0, value=f'{response[0]}\n', image=camp_img, compound='top')
                except HTTPError as err:
                    if err.code == 404:
                        break
                    else:
                        raise HTTPError("Error: Problem opening image")
                row_num += 1
            else:
                pass

        results_tabview._segmented_button._buttons_dict[f'Loading Campgrounds in {state_cd}...'].configure(text=f'Campgrounds in {state_cd}')

        

def get_hiking_response(state_cd, api_key):
    '''invoke thignstodo API and display results'''
     # check if results for that state were already retrieved
    try:
        results_tabview.set(f'Loading Hiking Trails in {state_cd}...')
    except:
        # get inputs and invoke apis
        hike_responses_formatted = get_thingstodo_response(state_cd, api_key)

        # create new tab and frame
        hike_result_tab = results_tabview.add(f'Loading Hiking Trails in {state_cd}...')
        results_tabview.set(f'Loading Hiking Trails in {state_cd}...')
        hiking_frame = customtkinter.CTkScrollableFrame(master=hike_result_tab, height=900, width=1300)
        hiking_frame.pack(padx=10, pady=10)

        # output results
        hike_headers = [['Trail', 'Description and URL', 'Duration', 'Trail Fees', 'Pets Allowed']]
        hike_results = CTkTable(master=hiking_frame, values=hike_headers, width=1, wraplength=500, header_color="#2A8C55")
        hike_results.edit_row(0, text_color="#fff", hover_color="#2A8C55")
        hike_results.pack(expand=True)

        row_num = 1
        for response in hike_responses_formatted:
            image_url = str(response.pop())
            hike_results.add_row(response)

            if image_url:
                try:
                    u = urlopen(image_url)
                    raw_data = u.read()
                    u.close()
                    imagetk = ImageTk.PhotoImage(data=raw_data)
                    image = ImageTk.getimage(imagetk)
                    trail_img = customtkinter.CTkImage(light_image=image, size=(150,150))
                    hike_results.insert(row=row_num, column=0, value=f'{response[0]}\n', image=trail_img, compound='top')
                except HTTPError as err:
                    if err.code == 404:
                        break
                    else:
                        raise HTTPError("Error: Problem opening image")
                row_num += 1
            else:
                pass

        results_tabview._segmented_button._buttons_dict[f'Loading Hiking Trails in {state_cd}...'].configure(text=f'Hiking Trails in {state_cd}')


def format_response():
    '''get inputs from dropdown, invoke APIs, and display results'''
    # get inputs and invoke apis
    state_cd = state_combo.get()
    activity = activity_combo.get()
    api_key = get_api_key()
    
    if activity == 'Camping':
        get_camping_response(state_cd, api_key)
    elif activity == 'Hiking':
        get_hiking_response(state_cd, api_key)

######################################################################################
# UI
######################################################################################
if __name__ == "__main__":
    # set theme and color options for customtkinter
    customtkinter.set_appearance_mode("light")
    customtkinter.set_default_color_theme("themes/Greengage.json")

    window = customtkinter.CTk()
    window.title('National Park Planner')
    window.resizable(width=False, height=False)


    # LEFT HALF -----------------------------------------------------------------------
    # create frame for greeting and logo
    logo_frame = customtkinter.CTkFrame(master=window)
    logo_frame.grid(row=0, column=0, padx=10, pady=10)

    greeting = customtkinter.CTkLabel(
        master=logo_frame,
        text="Welcome to the\nNational Park Planner!",
        font=('arial bold', 18)
    )
    greeting.pack(padx=10, pady=10)

    lbl_nps_logo = customtkinter.CTkImage(light_image=Image.open('./images/nps-logo.png'), size=(200,131))
    nps_logo = customtkinter.CTkLabel(master=logo_frame, text="", image=lbl_nps_logo)
    nps_logo.pack(padx=10, pady=10)

    desc_txt = customtkinter.CTkLabel(
        master=logo_frame,
        text="Choose a state and activity below to return the campgrounds and trails maintained by NPS to the right."
    )
    desc_txt._label.configure(wraplength=250)
    desc_txt.pack(padx=10, pady=10)

    # create frame for input
    input_frame = customtkinter.CTkFrame(master=window)
    input_frame.grid(row=1, column=0, padx=10, pady=10)

    lbl_state = customtkinter.CTkLabel(master=input_frame, text="State: ")
    lbl_state.grid(row=0, column=0, padx=10, pady=10)
    state_codes = get_state_cds()
    state_combo = customtkinter.CTkComboBox(master=input_frame, state='readonly', values=state_codes)
    state_combo.set('AK')
    state_combo.grid(row=0, column=1, padx=10, pady=10)

    lbl_activity = customtkinter.CTkLabel(master=input_frame, text="Activity: ")
    lbl_activity.grid(row=1, column=0, padx=10, pady=10)
    activities = ['Camping', 'Hiking']
    activity_combo = customtkinter.CTkComboBox(master=input_frame, state='readonly', values=activities)
    activity_combo.set('Camping')
    activity_combo.grid(row=1, column=1, padx=10, pady=10)

    submit_button = customtkinter.CTkButton(
        master=input_frame,
        text="Submit!",
        command=format_response,
        corner_radius=32
    )
    submit_button.grid(row=3, column=0, columnspan=2, padx=10, pady=10)


    # RIGHT HALF -----------------------------------------------------------------------
    # create tabview that will house result tabs
    results_tabview = customtkinter.CTkTabview(master=window, height=900, width=1300, anchor='w')
    results_tabview.grid(row=0, column=2, rowspan=15, padx=10, pady=10)


 
    master=window.mainloop()
